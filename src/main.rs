#![feature(destructuring_assignment)]

mod yy_2021;

fn main() -> std::io::Result<()> {
    if let Some(day) = std::env::args().nth(1) {
        match day.as_ref() {
            "1" => yy_2021::day_1::run(),
            "2" => yy_2021::day_2::run(),
            "3" => yy_2021::day_3::run(),
            "4" => yy_2021::day_4::run(),
            _ => {
                eprintln!("Day not yet solved.");
                Ok(())
            }
        }
    } else {
        eprintln!("Expected at least one argument");
        Ok(())
    }
}
