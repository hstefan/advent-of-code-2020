use std::io::{self, BufRead};

#[derive(PartialEq, Debug)]
enum Element {
    Tree,
    Space,
}

type Grid = Vec<Vec<Element>>;

fn parse_line(line: &str) -> Vec<Element> {
    line.chars()
        .map(|c| match c {
            '.' => Element::Space,
            '#' => Element::Tree,
            _ => panic!("shouldn't happen!"),
        }).collect()
}

fn descend_grid(grid: &Grid, (x, y): (usize, usize)) -> usize {
    let (rows, columns) = (grid.len(), grid[0].len());
    let (mut i, mut j) = (0, 0);
    let mut count = 0usize;
    loop {
        i += y;
        if i >= rows {
            break;
        }
        j = (j + x) % columns;
        if grid[i][j] == Element::Tree {
            count += 1;
        }
    }
    count
}

pub fn run() -> io::Result<()> {
    let stdin = io::stdin();
    let grid: Grid = stdin
        .lock()
        .lines()
        .map(|x| parse_line(&x.unwrap()))
        .collect();
    let slopes: [(usize, usize); 5] = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
    let mut total = 1;
    for slope in slopes.iter() {
        let trees = descend_grid(&grid, *slope);
        println!("Trees in slope {:?}: {:?}", slope, trees);
        total = total * trees;
    }
    println!("All counts multiplied: {}", total);
    Ok(())
}
