use std::collections::{HashSet, VecDeque};
use std::io::{self, BufRead};

#[derive(Debug, Copy, Clone)]
enum Instr {
    Noop(i32),
    Acc(i32),
    Jmp(i32),
}

type Program = Vec<Instr>;

fn execute(prog: &Program) {
    let mut acc = 0;
    let mut ptr = 0;
    let mut executed = HashSet::<i32>::new();
    loop {
        if executed.contains(&ptr) {
            println!("acc before loop: {}", acc);
            break;
        }
        executed.insert(ptr);
        ptr += match prog[ptr as usize] {
            Instr::Noop(_) => 1,
            Instr::Acc(n) => {
                acc += n;
                1
            }
            Instr::Jmp(n) => n,
        };
    }
}

fn execute_until_end(prog: &Program) {
    let mut acc = 0;
    let mut ptr = 0i32;
    let mut flip: Option<i32> = None;
    let mut candidates = VecDeque::<i32>::new();
    let mut executed = HashSet::<i32>::new();
    let mut attempted = HashSet::<i32>::new();
    let len = prog.len() as i32;
    loop {
        if ptr >= len {
            if let Some(flip) = flip {
                println!("just flip \'{:?}\' (Line {})", prog[flip as usize], flip);
            }
            println!("end found, acc: {}", acc);
            break;
        }
        if executed.contains(&ptr) {
            // this is eine loopity loop, reset and DO A KICK FLIP
            executed.clear();
            ptr = 0;
            acc = 0;
            if let Some(flip) = flip {
                attempted.insert(flip); // no dice
            }
            flip = candidates.pop_front();
        }
        executed.insert(ptr);
        let instr = if let Some(flip_idx) = flip {
            if flip_idx == ptr {
                match prog[ptr as usize] {
                    Instr::Noop(x) => Instr::Jmp(x),
                    Instr::Jmp(x) => Instr::Noop(x),
                    Instr::Acc(_x) => panic!("should not happen"),
                }
            } else {
                prog[ptr as usize]
            }
        } else {
            prog[ptr as usize]
        };
        ptr += match instr {
            Instr::Noop(_) => {
                if !attempted.contains(&ptr) {
                    candidates.push_back(ptr);
                }
                1
            }
            Instr::Acc(n) => {
                acc += n;
                1
            }
            Instr::Jmp(n) => {
                if !attempted.contains(&ptr) {
                    candidates.push_back(ptr);
                }
                n
            }
        };
    }
}

pub fn run() -> io::Result<()> {
    let stdin = io::stdin();
    let mut prog = Program::new();
    for line in stdin.lock().lines() {
        let line = line?;
        let mut line_split = line.split(" ");
        let (tag, val) = (
            line_split.next().unwrap(),
            line_split
                .next()
                .map(|x| x.parse::<i32>().unwrap())
                .unwrap(),
        );
        match tag {
            "nop" => prog.push(Instr::Noop(val)),
            "acc" => prog.push(Instr::Acc(val)),
            "jmp" => prog.push(Instr::Jmp(val)),
            _ => eprintln!("unknown instr {}", tag),
        }
    }
    execute(&prog);
    execute_until_end(&prog);
    Ok(())
}
