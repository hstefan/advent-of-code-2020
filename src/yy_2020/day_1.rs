use std::io::{self, BufRead};

fn part_one(all: &[i32]) -> Option<i32> {
    let len = all.len();
    for i in 0..len {
        for j in (i + 1)..len {
            if all[i] + all[j] == 2020 {
                let m = all[i] * all[j];
                println!("{} * {} = {}", all[i], all[j], m);
                return Some(m);
            }
        }
    }
    None
}

fn part_two(all: &[i32]) -> Option<i32> {
    let len = all.len();
    for i in 0..len {
        for j in (i + 1)..len {
            for k in (j + 1)..len {
                if all[i] + all[j] + all[k] == 2020 {
                    let m = all[i] * all[j] * all[k];
                    println!("{} * {} * {} = {}", all[i], all[j], all[k], m);
                    return Some(m);
                }
            }
        }
    }
    None
}

pub fn run() -> io::Result<()> {
    let stdin = io::stdin();
    let all = stdin
        .lock()
        .lines()
        .map(|x| x.unwrap().parse::<i32>().unwrap())
        .collect::<Vec<i32>>();
    part_one(&all);
    part_two(&all);
    Ok(())
}
