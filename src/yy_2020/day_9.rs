use std::io::{self, BufRead};

const PREAMBLE_LEN: usize = 25;

fn read_numbers<T: BufRead>(buf: &mut T) -> io::Result<Vec<i64>> {
    let mut numbers = Vec::<i64>::with_capacity(PREAMBLE_LEN);
    for line in buf.lines() {
        numbers.push(line?.parse().unwrap());
    }
    Ok(numbers)
}

fn part_one(numbers: &Vec<i64>) -> Option<usize> {
    'outer: for i in PREAMBLE_LEN..numbers.len() {
        let (start, end) = (i - PREAMBLE_LEN, i);
        for j in start..end {
            for k in (j + 1)..end {
                if (numbers[j] + numbers[k]) == numbers[i] {
                    continue 'outer;
                }
            }
        }
        return Some(i);
    }

    None // should be an error, whatever
}

fn part_two(numbers: &Vec<i64>, idx: usize) -> Option<i64> {
    let (mut i, mut j) = (0usize, 0usize);
    let mut acc = numbers[i];
    while acc != numbers[idx] {
        j += 1;
        acc += numbers[j];
        while acc > numbers[idx] {
            acc -= numbers[i];
            i += 1;
        }
        assert!(i < j);
    }

    // we could do it in the above loop, but meh
    let (mut min, mut max) = (i64::MAX, i64::MIN);
    print!("[");
    for k in i..(j + 1) {
        print!("{},", numbers[k]);
        if numbers[k] < min {
            min = numbers[k];
        }
        if numbers[k] > max {
            max = numbers[k];
        }
    }
    println!("]");
    Some(min + max)
}

pub fn run() -> io::Result<()> {
    let stdin = io::stdin();
    let mut lock = stdin.lock();
    let numbers = read_numbers(&mut lock)?;
    let idx_first = part_one(&numbers).unwrap();
    println!("first invalid number: {:?}", numbers[idx_first]);
    println!("part two: {:?}", part_two(&numbers, idx_first));
    Ok(())
}
