use std::io::{self, BufRead};
use std::collections::HashMap;

struct PortSys {
    pub r_mask: u64,
    pub s_mask: u64,
    pub mem: HashMap<u64, u64>
}

fn part_one<T: BufRead>(buf: &mut T) -> io::Result<()> {
    let (mut s_mask, mut r_mask) = (0u64, !0u64);
    for line in buf.lines() {
        let line = line?;
        if line.starts_with("mask") {
            let mask = &line["mask = ".len()..];
            s_mask = u64::from_str_radix(&mask.replace("X", "0"), 2).unwrap();
            r_mask = !0u64 | u64::from_str_radix(&mask.replace("X", "1"), 2).unwrap();
        } else {
        }
    } 
    Ok(())
}

pub fn run() -> io::Result<()> {
    let stdin = io::stdin();
    let mut lock =  stdin.lock();
    part_one(&mut lock);
    Ok(())
}