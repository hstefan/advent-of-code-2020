use std::collections::HashMap;
use std::io::{self, BufRead};

type BagColor = String;

type RuleSet = HashMap<BagColor, HashMap<BagColor, i32>>;

const MY_BAG: &'static str = "shiny gold";

fn can_contain_shiny_bag(bag: &str, ruleset: &RuleSet) -> bool {
    if let Some(children) = ruleset.get(bag) {
        for (allowed_bag, _) in children.iter() {
            if allowed_bag == MY_BAG {
                return true;
            }
            if can_contain_shiny_bag(allowed_bag, ruleset) {
                return true;
            }
        }
    }
    false
}

fn count_times_found(ruleset: &RuleSet) -> i32 {
    let mut count = 0;
    for bag in ruleset.keys().cloned() {
        if can_contain_shiny_bag(&bag, ruleset) {
            count += 1;
        }
    }
    count
}

fn how_many_bags_in(ruleset: &RuleSet, bag: &str) -> i32 {
    let mut count = 1;
    for (child, n) in &ruleset[bag] {
        count += n * how_many_bags_in(ruleset, &child);
    }
    count
}

pub fn run() -> io::Result<()> {
    // key is the color, value is what bags can contain it and how many
    let mut rules = RuleSet::new();
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let line = line?;
        let outter: Vec<&str> = line.split("bags contain").map(|s| s.trim()).collect();
        assert_eq!(outter.len(), 2);
        let mut r = HashMap::<BagColor, i32>::new();
        for contents in outter[1].split(",").map(|s| s.trim()) {
            if contents.starts_with("no other bags.") {
                break;
            }
            let contents: Vec<&str> = contents.split(" ").collect();
            let count = contents[0].parse::<i32>().unwrap();
            let color = format!("{} {}", contents[1], contents[2]);
            r.insert(color, count);
        }
        rules.insert(outter[0].to_owned(), r);
    }
    println!("shiny_gold fits in {} bags", count_times_found(&rules));
    println!(
        "shiny_gold has to fit {} bags",
        how_many_bags_in(&rules, MY_BAG) - 1
    );
    Ok(())
}
