use std::collections::HashSet;
use std::io::{self, BufRead};

fn part_one() -> io::Result<i32> {
    let stdin = io::stdin();
    let mut total = 0;
    let mut fields_found: HashSet<String> = HashSet::new();
    for line in stdin.lock().lines() {
        let line = line?;
        let line = line.trim();
        if line.len() <= 1 {
            // reached an empty line
            if fields_found.len() == 7 {
                total += 1;
            }
            fields_found.clear();
            continue;
        }
        for field in line.split(' ').map(|x| x.split(':').nth(0).unwrap()) {
            if fields_found.contains(field) {
                continue;
            }
            match field {
                "byr" | "iyr" | "eyr" | "hgt" | "hcl" | "ecl" | "pid" => {
                    fields_found.insert(field.to_owned());
                }
                _ => continue,
            }
        }
    }
    if fields_found.len() == 7 {
        total += 1;
    }

    Ok(total)
}

fn field_in_range(value: &str, digits: usize, min: i32, max: i32) -> bool {
    if value.len() != digits {
        return false;
    }
    if let Ok(num) = value.parse::<i32>() {
        if num >= min && num <= max {
            return true;
        }
    }
    false
}

fn valid_height(value: &str) -> bool {
    if value.len() < 4 {
        return false;
    }
    if value.ends_with("cm") {
        if let Ok(v) = value[0..(value.len() - 2)].parse::<i32>() {
            if v >= 150 && v <= 193 {
                return true;
            }
        }
    } else if value.ends_with("in") {
        if let Ok(v) = value[0..(value.len() - 2)].parse::<i32>() {
            if v >= 59 && v <= 76 {
                return true;
            }
        }
    }
    false
}

fn valid_eye_color(value: &str) -> bool {
    match value {
        "amb" | "blu" | "brn" | "gry" | "grn" | "hzl" | "oth" => true,
        _ => false,
    }
}

fn valid_hair_color(value: &str) -> bool {
    if value.len() != 7 || !value.starts_with("#") {
        return false;
    }
    value.chars().skip(1).all(|x| match x {
        'a' | 'b' | 'c' | 'd' | 'e' | 'f' => true,
        v => v.is_digit(10),
    })
}

fn valid_passport_id(value: &str) -> bool {
    if value.len() != 9 {
        return false;
    }
    value.chars().all(|x| x.is_digit(10))
}

fn part_two() -> io::Result<()> {
    let stdin = io::stdin();
    let mut total = 0;
    let mut fields_found: HashSet<String> = HashSet::new();
    let mut invalid = false;
    for line in stdin.lock().lines() {
        let line = line?;
        let line = line.trim();
        if line.len() <= 1 {
            // reached an empty line
            print!("END PASSPORT: ");
            if fields_found.len() >= 7 {
                println!("VALID!");
                total += 1;
            } else {
                println!("INVALID!");
            }
            fields_found.clear();
            invalid = false;
            continue;
        }
        if invalid {
            continue;
        }
        for p in line.split(' ').map(|x| x.split(':').collect::<Vec<&str>>()) {
            let (field, value) = (p[0], p[1]);
            if fields_found.contains(field) {
                continue;
            }
            let (is_field, inv) = match field {
                "byr" => (true, !field_in_range(value, 4, 1920, 2002)),
                "iyr" => (true, !field_in_range(value, 4, 2010, 2020)),
                "eyr" => (true, !field_in_range(value, 4, 2020, 2030)),
                "hgt" => (true, !valid_height(value)),
                "hcl" => (true, !valid_hair_color(value)),
                "ecl" => (true, !valid_eye_color(value)),
                "pid" => (true, !valid_passport_id(value)),
                _ => (false, false),
            };
            invalid = inv;
            if invalid {
                println!("invalid {}:{}", field, value);
                break;
            } else if is_field {
                fields_found.insert(field.to_owned());
            }
        }
    }
    if fields_found.len() >= 7 && !invalid {
        println!("it added here");
        total += 1;
    }

    println!("Total valid passports: {}", total);

    Ok(())
}

pub fn run() -> io::Result<()> {
    part_two()
}
