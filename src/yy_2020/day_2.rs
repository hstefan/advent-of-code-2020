use regex::Regex;
use std::io::{self, BufRead};

fn password_valid_1(password: &str, letter: &char, min: usize, max: usize) -> bool {
    let mut count = 0;
    for ch in password.chars() {
        if ch == *letter {
            count += 1;
            if count > max {
                return false;
            }
        }
    }
    count >= min
}

fn password_valid_2(password: &str, letter: &char, i: usize, j: usize) -> bool {
    let x = password.chars().nth(i - 1).unwrap();
    let y = password.chars().nth(j - 1).unwrap();
    let letter = *letter;
    (x == letter && y != letter) || (y == letter && x != letter)
}

pub fn run() -> io::Result<()> {
    let stdin = io::stdin();
    //1-3 a: abcde
    let re = Regex::new(r"^(?P<min>\d+)-(?P<max>\d+) (?P<char>\w): (?P<password>.*)$").unwrap();
    let mut count_1 = 0;
    let mut count_2 = 0;
    for line in stdin.lock().lines() {
        if let Some(parsed) = re.captures(&line?) {
            let min = parsed["min"].parse::<usize>().unwrap();
            let max = parsed["max"].parse::<usize>().unwrap();
            let ch = &parsed["char"].chars().nth(0).unwrap();
            let password = &parsed["password"];
            if password_valid_1(password, ch, min, max) {
                count_1 += 1;
            }
            if password_valid_2(password, ch, min, max) {
                count_2 += 1;
            }
        }
    }
    println!("count_1: {}", count_1);
    println!("count_2: {}", count_2);
    Ok(())
}
