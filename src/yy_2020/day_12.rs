use std::io::{self, BufRead};

enum Action {
    North(i32),
    South(i32),
    East(i32),
    West(i32),
    Forward(i32),
    RotLeft(i32),
    RotRight(i32),
}

fn read_instructions<T: BufRead>(buf: &mut T) -> io::Result<Vec<Action>> {
    let mut v = Vec::new();
    for line in buf.lines() {
        let line = line?;
        let num = (&line[1..]).parse().unwrap();
        let action = match &line[0..1] {
            "N" => Action::North(num),
            "S" => Action::South(num),
            "E" => Action::East(num),
            "W" => Action::West(num),
            "F" => Action::Forward(num),
            "L" => Action::RotLeft(num),
            "R" => Action::RotRight(num),
            _ => panic!("unexpected!"),
        };
        v.push(action)
    }
    Ok(v)
}

fn part_one(instructions: &[Action]) -> i32{
    let (mut x, mut y) = (0, 0);
    let (mut dx, mut dy) = (1, 0);
    for instr in instructions {
        match instr {
            Action::North(v) => y += v,
            Action::South(v) => y -= v,
            Action::East(v) => x += v,
            Action::West(v) => x -= v,
            Action::Forward(v) => {
                x += dx * v;
                y += dy * v;
            },
            Action::RotLeft(v) => {
                for _ in 0..(v/90) {
                    (dx, dy) = (-dy, dx);
                }
            },
            Action::RotRight(v) => {
                for _ in 0..(v/90) {
                    (dx, dy) = (dy, -dx);
                }
            }
        }
    }
    x.abs() + y.abs()
}

fn part_two(instructions: &[Action]) -> i32 {
    let (mut x, mut y) = (0, 0);
    let (mut dx, mut dy) = (10, 1);
    for instr in instructions {
        match instr {
            Action::North(v) => dy += v,
            Action::South(v) => dy -= v,
            Action::East(v) => dx += v,
            Action::West(v) => dx -= v,
            Action::Forward(v) => {
                x += dx * v;
                y += dy * v;
            },
            Action::RotLeft(v) => {
                for _ in 0..(v/90) {
                    (dx, dy) = (-dy, dx);
                }
            },
            Action::RotRight(v) => {
                for _ in 0..(v/90) {
                    (dx, dy) = (dy, -dx);
                }
            }
        }
    }
    x.abs() + y.abs()
}

pub fn run() -> io::Result<()> {
    let stdin =io::stdin();
    let mut lock = stdin.lock();
    let instructions = read_instructions(&mut lock)?;
    println!("[1] manhatan dist: {}", part_one(&instructions));
    println!("[2] manhatan dist: {}", part_two(&instructions));
    Ok(())
}