use std::io::{self, BufRead};

#[derive(Debug)]
struct Schedule {
    arrival: i32,
    busses: Vec<(i32, i32)>,
}

fn read_schedule<T: BufRead>(buf: &mut T) -> io::Result<Schedule> {
    let mut lines = buf.lines();
    let arrival = lines.next().unwrap()?.parse::<i32>().unwrap();
    let busses = lines
        .next()
        .unwrap()?
        .split(",")
        .enumerate()
        .filter(|(_, c)| *c != "x")
        .map(|(n, x)| (n as i32, x.parse::<i32>().unwrap()))
        .collect::<Vec<(i32, i32)>>();
    Ok(Schedule { arrival, busses })
}

fn part_one(schedule: &Schedule) -> i32 {
    let (mut best_bus, mut min_wait) = (0, i32::MAX);
    for (_, bus) in &schedule.busses {
        let closest = (schedule.arrival / bus) * bus;
        let earliest = if closest < schedule.arrival {
            closest + bus
        } else {
            closest
        };
        let wait = earliest - schedule.arrival;
        if wait < min_wait {
            best_bus = *bus;
            min_wait = wait;
        }
    }
    best_bus * min_wait
}

fn part_two(schedule: &Schedule) -> u128 {
    let mut ts = 0;
    loop {
        let mut offset = 1;
        let mut valid = true;
        for (idx, bus) in &schedule.busses {
            if (ts + (*idx as u128)) % (*bus as u128) != 0 {
                valid = false;
                break;
            }
            offset *= *bus as u128;
        }
        if valid {
            break;
        }
        ts += offset;
    }
    ts
}

pub fn run() -> io::Result<()> {
    let stdin = io::stdin();
    let mut lock = stdin.lock();
    let schedule = read_schedule(&mut lock)?;
    println!("{:?}", schedule);
    println!("part one: {}", part_one(&schedule));
    println!("part two: {}", part_two(&schedule));
    Ok(())
}
