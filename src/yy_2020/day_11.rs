use std::io::{self, BufRead};

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum Elem {
    Empty,
    Occupied,
    Floor,
}

type SeatPlan = Vec<Elem>;

fn parse_seatplan<T: BufRead>(buf: &mut T) -> io::Result<(i64, i64, SeatPlan)> {
    let (mut rows, mut columns) = (0, 0);
    let mut seatplan = SeatPlan::new();
    for line in buf.lines() {
        let line = line?;
        rows += 1;
        columns = line.len();
        seatplan.extend(line.chars().map(|x| match x {
            '.' => Elem::Floor,
            'L' => Elem::Empty,
            '#' => Elem::Occupied,
            _ => panic!("shouldn't happen"),
        }))
    }
    Ok((rows as i64, columns as i64, seatplan))
}

fn idx(row: i64, column: i64, num_columns: i64) -> usize {
    (row * num_columns + column) as usize
}

fn count_adjacent_neighbors(
    seatplan: &SeatPlan,
    row: i64,
    column: i64,
    rows: i64,
    columns: i64,
    max: usize,
) -> usize {
    let mut neighbors = 0usize;
    'outer: for i in -1..2 {
        let r = row + i;
        if r < 0 || r >= rows {
            continue;
        }
        for j in -1..2 {
            let c = column + j;
            if c < 0 || c >= columns || (j == 0 && i == 0) {
                continue;
            }
            if seatplan[idx(r, c, columns)] == Elem::Occupied {
                neighbors += 1;
                if neighbors >= max {
                    break 'outer;
                }
            }
        }
    }
    neighbors
}

fn neighbors_in_sight(
    seatplan: &SeatPlan,
    row: i64,
    column: i64,
    rows: i64,
    columns: i64,
    max: usize,
) -> usize {
    let mut neighbors = 0usize;
    let dirs = [
        (-1, -1),
        (-1, 0),
        (-1, 1),
        (0, -1),
        (0, 1),
        (1, -1),
        (1, 0),
        (1, 1),
    ];
    for (dr, dc) in dirs.iter() {
        if neighbors >= max {
            break;
        }
        let (mut r, mut c) = (row, column);
        loop {
            r = r + dr;
            c = c + dc;
            if r < 0 || r >= rows || c < 0 || c >= columns {
                break;
            }

            match seatplan[idx(r, c, columns)] {
                Elem::Occupied => {
                    neighbors += 1;
                    break;
                }
                Elem::Empty => break,
                _ => (),
            }
        }
    }
    neighbors
}

fn simulate(seatplan: &mut SeatPlan, rows: i64, columns: i64) -> usize {
    let snapshot = seatplan.clone();
    let mut num_updates = 0;
    for i in 0..rows {
        for j in 0..columns {
            let s_idx = idx(i, j, columns);
            match snapshot[s_idx] {
                Elem::Empty => {
                    if count_adjacent_neighbors(&snapshot, i, j, rows, columns, 1) < 1 {
                        seatplan[s_idx] = Elem::Occupied;
                        num_updates += 1;
                    }
                }
                Elem::Occupied => {
                    if count_adjacent_neighbors(&snapshot, i, j, rows, columns, 4) >= 4 {
                        seatplan[s_idx] = Elem::Empty;
                        num_updates += 1;
                    }
                }
                _ => (),
            }
        }
    }
    num_updates
}

fn simulate_v2(seatplan: &mut SeatPlan, rows: i64, columns: i64) -> usize {
    let snapshot = seatplan.clone();
    let mut num_updates = 0;
    for i in 0..rows {
        for j in 0..columns {
            let s_idx = idx(i, j, columns);
            match snapshot[s_idx] {
                Elem::Empty => {
                    if neighbors_in_sight(&snapshot, i, j, rows, columns, 1) < 1 {
                        seatplan[s_idx] = Elem::Occupied;
                        num_updates += 1;
                    }
                }
                Elem::Occupied => {
                    if neighbors_in_sight(&snapshot, i, j, rows, columns, 5) >= 5 {
                        seatplan[s_idx] = Elem::Empty;
                        num_updates += 1;
                    }
                }
                _ => (),
            }
        }
    }
    num_updates
}

fn pretty_print(seatplan: &mut SeatPlan, rows: i64, columns: i64) {
    for i in 0..rows {
        for j in 0..columns {
            let c = match seatplan[idx(i, j, columns)] {
                Elem::Empty => "L",
                Elem::Occupied => "#",
                Elem::Floor => ".",
            };
            print!("{}", c);
        }
        println!();
    }
}

fn part_one(seatplan: &mut SeatPlan, rows: i64, columns: i64) -> usize {
    while simulate(seatplan, rows, columns) != 0 {}
    seatplan
        .iter()
        .fold(0, |acc, x| if *x == Elem::Occupied { acc + 1 } else { acc })
}

fn part_two(seatplan: &mut SeatPlan, rows: i64, columns: i64) -> usize {
    while simulate_v2(seatplan, rows, columns) != 0 {}
    seatplan
        .iter()
        .fold(0, |acc, x| if *x == Elem::Occupied { acc + 1 } else { acc })
}

pub fn run() -> io::Result<()> {
    let stdin = io::stdin();
    let mut lock = stdin.lock();
    let (rows, columns, mut seatplan) = parse_seatplan(&mut lock)?;
    println!("[initial plan]");
    pretty_print(&mut seatplan, rows, columns);
    {
        let mut v1 = seatplan.clone();
        println!(
            "[v1] number of occupied seats: {}",
            part_one(&mut v1, rows, columns)
        );
        pretty_print(&mut v1, rows, columns);
    }
    {
        let mut v2 = seatplan.clone();
        println!(
            "[v2] number of occupied seats: {}",
            part_two(&mut v2, rows, columns)
        );
        pretty_print(&mut v2, rows, columns);
    }
    Ok(())
}
