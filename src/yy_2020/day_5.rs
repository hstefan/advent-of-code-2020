use std::io::{self, BufRead};

pub fn run() -> io::Result<()> {
    let stdin = io::stdin();
    let mut seats = std::collections::HashSet::new();
    let (mut min, mut max) = (100000000, -1);
    for line in stdin.lock().lines() {
        let line = line?;
        assert_eq!(line.len(), 10);
        let mut row = 0;
        for (i, c) in line[0..7].chars().rev().enumerate() {
            if c == 'B' {
                row += 2i32.pow(i as u32);
            }
        }
        let mut column = 0;
        for (i, c) in line[7..10].chars().rev().enumerate() {
            if c == 'R' {
                column += 2i32.pow(i as u32);
            }
        }
        let seat_id = row * 8 + column;
        seats.insert(seat_id);
        if seat_id > max {
            max = seat_id;
        }
        if seat_id < min {
            min = seat_id;
        }
    }

    println!("max seat id: {}", max);

    for i in min..max {
        if !seats.contains(&i) {
            if seats.contains(&(i + 1)) && seats.contains(&(i - 1)) {
                println!("my seat: {}", i);
                break;
            }
        }
    }
    Ok(())
}
