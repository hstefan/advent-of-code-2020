use std::collections::{HashMap, HashSet};
use std::io::{self, BufRead};

fn read_jolts<T: BufRead>(buf: &mut T) -> HashSet<i64> {
    buf.lines()
        .map(|x| x.unwrap().parse::<i64>().unwrap())
        .collect()
}

fn part_one(v: &mut HashSet<i64>) -> (usize, usize) {
    let mut v = v.clone();
    let mut diffs = HashMap::<i64, usize>::new();
    diffs.insert(3, 1);
    part_one_inner(&mut v, &mut diffs, 0);
    (
        diffs.get(&1).cloned().unwrap_or_default(),
        diffs.get(&3).cloned().unwrap_or_default(),
    )
}

fn part_one_inner(v: &mut HashSet<i64>, diffs: &mut HashMap<i64, usize>, charge: i64) {
    for i in 1..4 {
        let adapter_charge = charge + i;
        if v.remove(&(adapter_charge)) {
            if let Some(count) = diffs.get_mut(&i) {
                *count = *count + 1;
            } else {
                diffs.insert(i, 1);
            }
            part_one_inner(v, diffs, adapter_charge);
        }
    }
}

pub fn run() -> io::Result<()> {
    let stdin = io::stdin();
    let mut lock = stdin.lock();
    let mut jolts = read_jolts(&mut lock);
    let (ones, threes) = part_one(&mut jolts);
    println!("product of 1 and 3 jolt differences: {:?}", ones * threes);
    Ok(())
}
