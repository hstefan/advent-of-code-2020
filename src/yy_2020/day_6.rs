use std::collections::HashSet;
use std::io::{self, BufRead};

fn part_one() -> std::io::Result<()> {
    let stdin = io::stdin();
    let mut yes_answers: HashSet<char> = HashSet::new();
    let mut total = 0;
    for line in stdin.lock().lines() {
        let line = line?;
        if line.len() == 0 {
            total += yes_answers.len();
            yes_answers.clear();
        } else {
            for c in line.chars() {
                if !yes_answers.contains(&c) {
                    yes_answers.insert(c);
                }
            }
        }
    }
    total += yes_answers.len();
    println!("Number of yes answers: {}", total);
    Ok(())
}

fn part_two() -> std::io::Result<()> {
    let stdin = io::stdin();
    let mut yes_answers = HashSet::<char>::new();
    let mut init = false;
    let mut total = 0;
    for line in stdin.lock().lines() {
        let line = line?;
        if line.len() == 0 {
            total += yes_answers.len();
            yes_answers.clear();
            init = false;
            println!("");
        } else {
            let mut person = HashSet::<char>::new();
            for c in line.chars() {
                if !person.contains(&c) {
                    person.insert(c);
                }
            }
            if !init {
                yes_answers = person;
                init = true;
            } else {
                yes_answers = yes_answers.intersection(&person).copied().collect();
            }
        }
    }
    total += yes_answers.len();
    println!("Number of all yes answers: {}", total);
    Ok(())
}

pub fn run() -> std::io::Result<()> {
    part_two()
}
