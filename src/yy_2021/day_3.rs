use std::io::{self, BufRead};
use std::collections::HashSet;

fn part_one(measurements: &[String]) -> i32 {
    let mut counts = Vec::<(i32, i32)>::new();
    for meas in measurements {
        if counts.len() == 0 {
            counts.resize(meas.len(), (0, 0));
        }
        let len = counts.len() - 1;
        for (i, c) in meas.chars().enumerate() {
            let (ref mut zeroes, ref mut ones) = counts[len - i];
            match c {
                '0' => *zeroes += 1,
                '1' => *ones += 1,
                _ => panic!("wtf")
            }
        }
    }
    let (mut gamma, mut epsilon) = (0, 0);
    let mut mask = 0b1;
    for (zeroes, ones) in &counts {
        if ones > zeroes {
            gamma |= mask;
        } else {
            epsilon |= mask;
        }
        mask <<= 1;
    }
    println!("gamma: {}, epsilon: {}", gamma, epsilon);
    println!("res 1: {}", gamma * epsilon);
    gamma * epsilon
}

fn find_oxygen(mut oxygen: HashSet<String>, len: usize) -> usize {
    for i in 0..len {
        let (mut ones, mut zeros) = (0, 0);
        for m in &oxygen {
            match m.chars().nth(i) {
                Some('0') => zeros += 1,
                Some('1') => ones += 1,
                _ => panic!("wtf")
            }
        }
        let most_common = if ones >= zeros  { '1' } else { '0' };
        let mut filtered = Vec::<String>::new();
        for m in &oxygen {
            let c = m.chars().nth(i).unwrap();
            if c != most_common && oxygen.len() > 1 {
                filtered.push(m.clone());
            }
        }
        for f in filtered {
            oxygen.remove(&f);
        }
        if oxygen.len() == 1 {
            break;
        }
    }
    usize::from_str_radix(&oxygen.iter().nth(0).unwrap(), 2).unwrap()
}

fn find_scrub(mut scrubs: HashSet<String>, len: usize) -> usize {
    for i in 0..len {
        let (mut ones, mut zeros) = (0, 0);
        for m in &scrubs {
            match m.chars().nth(i) {
                Some('0') => zeros += 1,
                Some('1') => ones += 1,
                _ => panic!("wtf")
            }
        }
        let least_common = if ones >= zeros  { '0' } else { '1' };
        let mut filtered = Vec::<String>::new();
        for m in &scrubs {
            let c = m.chars().nth(i).unwrap();
            if c != least_common && scrubs.len() > 1 {
                filtered.push(m.clone());
            }
        }
        for f in filtered {
            scrubs.remove(&f);
        }
        if scrubs.len() == 1 {
            break;
        }
    }
    usize::from_str_radix(&scrubs.iter().nth(0).unwrap(), 2).unwrap()
}
fn part_two(measurements: &[String]) -> i32 {
    let oxygen: HashSet<_> = measurements.iter().cloned().collect();
    let scrubber: HashSet<_> = measurements.iter().cloned().collect();
    let len = measurements[0].len();
    println!("ox: {:?}", find_oxygen(oxygen, len));
    println!("scrub: {:?}", find_scrub(scrubber, len));
    0
}

pub fn run() -> io::Result<()> {
    let stdin = io::stdin();
    let all = stdin
        .lock()
        .lines()
        .map(|x| x.unwrap())
        .collect::<Vec<String>>();
    part_one(&all);
    part_two(&all);
    Ok(())
}
