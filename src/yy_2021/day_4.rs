use std::hash::Hash;
use std::io::{self, BufRead};
use std::collections::HashSet;

#[derive(Debug)]
struct Board {
    pub rows: Vec<HashSet<i32>>,
    pub marked: Vec<HashSet<i32>>,
}

fn part_one(nums: Vec<i32>, mut boards: Vec<Board>) -> i32 {
    for num in nums {
        for board in &mut boards {
            for i in 0..board.rows.len() {
                if board.rows[i].contains(&num) {
                    board.marked[i].insert(num);
                    if board.marked[i].len() == board.rows[i].len() {
                        let mut sum = 0;
                        for j in 0..board.rows.len() {
                            for n in &board.rows[j] {
                                if !board.marked[j].contains(&n) {
                                    sum += n;
                                }
                            }
                        }
                        println!("score: {}", sum * num);
                        return 0
                    }
                }
            } 
        }
    }
    0
}

fn part_two(measurements: &[String]) -> i32 {
    0
}

pub fn run() -> io::Result<()> {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines().map(|x| x.unwrap());
    let nums: Vec<i32> = lines.next().unwrap().split(",").map(|x| x.parse::<i32>().unwrap()).collect();
    let mut boards = Vec::<Board>::new();
    let mut rows = Vec::<HashSet<i32>>::new();
    let _ = lines.next();
    
    while let Some(line)  = lines.next() {
        if line.len() == 0 {
            let mut marked = Vec::<HashSet<i32>>::new();
            marked.resize(rows.len(), HashSet::<i32>::new());
            let board = Board {rows: rows.clone(), marked: marked};
            boards.push(board);
            rows.clear();
            continue;
        }
        rows.push(line.split(" ").map(|x| x.parse::<i32>()).filter_map(|x| x.ok()).collect());
    }
    let mut marked = Vec::<HashSet<i32>>::new();
    marked.resize(rows.len(), HashSet::<i32>::new());
    let board = Board {rows: rows.clone(), marked: marked};
    boards.push(board);

    println!("boards: {:?}", boards);
    part_one(nums, boards);
    Ok(())
}
