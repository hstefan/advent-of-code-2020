use std::io::{self, BufRead};

fn part_one(all: &[i32]) -> i32 {
    let len = all.len();
    let mut cnt = 0;
    for i in 1..len {
        if all[i] > all[i - 1] {
            cnt += 1;
        }
    }
    println!("cnt: {}", cnt);
    cnt
}

fn part_two(all: &[i32]) -> i32 {
    let len = all.len();
    let mut cnt = 0;
    let mut last_sum = -1;
    for i in 0..(len - 2) {
        let new_sum = all[i] + all[i + 1] + all[i + 2];
        if last_sum == -1 {
            last_sum = new_sum;
            println!("n/a");
            continue;
        }
        if new_sum > last_sum {
            println!("increased!");
            cnt += 1;
        } else if new_sum < last_sum {
            println!("decreased");
        } else {
            println!("no change");
        }
        last_sum = new_sum;
    }
    println!("cnt: {}", cnt);
    cnt
}

pub fn run() -> io::Result<()> {
    let stdin = io::stdin();
    let all = stdin
        .lock()
        .lines()
        .map(|x| x.unwrap().parse::<i32>().unwrap())
        .collect::<Vec<i32>>();
    part_one(&all);
    part_two(&all);
    Ok(())
}
 