use std::io::{self, BufRead};

fn part_one(directions: &[(String, i32)]) -> i32 {
    let (mut horizontal, mut depth) = (0, 0);
    for (cmd, val) in directions {
        match cmd.as_str() {
            "forward" => horizontal += val,
            "down" => depth += val,
            "up" => depth -= val,
            _ => println!("unsupported")
        }
    }
    println!("part_one: {}", horizontal * depth);
    horizontal * depth
}

fn part_two(directions: &[(String, i32)]) -> i32 {
    let (mut horizontal, mut depth, mut aim) = (0, 0, 0);
    for (cmd, val) in directions {
        match cmd.as_str() {
            "forward" => {
                horizontal += val;
                depth += aim * val;
            },
            "down" => aim += val,
            "up" => aim -= val,
            _ => println!("unsupported")
        }
    }
    println!("part_two: {}", horizontal * depth);
    horizontal * depth
}

pub fn run() -> io::Result<()> {
    let stdin = io::stdin();
    let all = stdin
        .lock()
        .lines()
        .map(|x| {
            let x = x.unwrap();
            let s = x.split(" ").collect::<Vec<&str>>();
            (s[0].to_owned(), s[1].parse::<i32>().unwrap())
        })
        .collect::<Vec<(String, i32)>>();
    part_one(&all);
    part_two(&all);
    Ok(())
}
 